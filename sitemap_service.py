# Gereksinimler
import xmltodict
import json
import requests
from models.status import getStatus
from models.database import getTable
from product_service import addProduct

sitemaps = getTable("sitemaps")
# Veritabanı üzerinde SiteMap tablosu üzerinde bulunan
# sitemap URL adreslerindeki dosya içeriğinin okunması
# okunan ürün kayıtlarının Products tablosu üzerine
# kayıt işlemleri yapılmaktadır


def httpXmlToJson(url):
    # Belirtilen URL bilgisine istekte bulunur
    # Gelen datanın bir XML data olduğunu varsayarak
    # sonrasında JSON dataya çevirerek geri döndürüyoruz
    content = requests.get(url)
    website_text = content.text
    webJson = json.dumps(xmltodict.parse(website_text))
    urlset = json.loads(webJson)
    return urlset


def startSitemapServiceAsync():
    print('Started Sitemap Service')
    # Fonksiyon çağırıldığı anda, bu alanda çağırılan fonksiyonlar çalıştırılıyor

    # Sitemap tablosundan veriler çekiliyor
    getSitemaps()
    pass


def getSitemaps():
    # Veritabanına bağlan
    # Sitemap tablosundan verileri getir
    # Her 10 sitemap kaydını alabiliriz, bu daha fazla da olabilir
    # Veritabanından çekilen urller o gün taranmış datalardan olmamalı
    # Her bir kayıt readSitemapFile nesnesine gönderilmeli
    # Gelen data for döngüsüyle işlenecek
    pass


def readSitemapFileFromURL(sitemapURL):
    # Bu fonksiyon ilgili bağlantıdan sadece xml dosyalarını tarar
    # Bulduğu her yeni xml dosyası içerisine tekrar bağlanır
    # Ta ki ürün bağlantısı bulana kadar
    # Bulduğu noktada ise ürün bulma fonksiyonuna iletir
    data_set = httpXmlToJson(sitemapURL)
    # XML data listesi
    data_index = [] if 'sitemapindex' not in data_set else data_set['sitemapindex']['sitemap']
    # Product data listesi
    data_product = [] if 'urlset' not in data_set else data_set['urlset']['url']

    # Bu bir XML listesiyse, yani varsa
    if len(data_index) > 0:
        for xml in data_index:
            xml_url = xml['loc']
            # Her bir kaydı veritabanına kaydet
            addSitemap(xml_url)
            # Bulduğun URL bilgisine bağlanarak tekrar tarat
            readSitemapFileFromURL(xml_url)

    # Ürün ile ilgili kayıtları diğer fonksiyona gönderiyoruz
    if len(data_product) > 0:
        findProductURLFromXmlData(data_product)


def findProductURLFromXmlData(data):
    # Gelen her bir ürün URL bilgisini veritabanına kaydedelim
    # Gelen url bir ürüne ait de olabilir, bir kurumsal sayfa da olabilir
    # Biz her şekilde kaydediyoruz
    # Taratma sırasında veritabanında ilgili alanlara, bu url bilgisinin gerçekten
    # bir ürün sayfası olup olmadığına dair durum bilgisi vereceğiz
    for product in data:
        addProduct(product['loc'])


def deleteSitemap(sitemapURL):
    # SitemapURL bilgisine ait kayıt veritabanından silinir
    pass


def addSitemap(sitemapURL):
    # Sitemap URL bilgisi eğer veritabanı tablosunda yoksa kaydediyoruz

    one = sitemaps.find_one({"url": sitemapURL})
    # Varsayılan olarak eklenemedi bilgisi için ID değeri 0
    _id = 0

    # Herhangi bir kayıt yoksa ekle
    if one is None:
        _id = sitemaps.insert_one({"url": sitemapURL}).inserted_id

    # Sonuca göre console üzerine mesaj göster
    getStatus(102) if _id == 0 else getStatus(100)

    # Sonuç kaydını döndür
    return _id
