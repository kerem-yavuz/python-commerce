# Global Service
# Bu servisin amacı, local service sınıfının hizmetinin aksi yönünde çalışmaktadır
# Bu servis, dış bir URL veya servisten çekilecek verileri
# kontrolleri ve gönderim işlemlerini icra etmektedir.
# Kısaca dışarıya yönelik işlemlerde bu servisi kullanacağız