import pymongo

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["commerce"]
def getConnection():
    return mydb
def getTable(name):
    return mydb[name]