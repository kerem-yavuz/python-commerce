status = dict({
    "0x0": "",
    "0x100": "%s bilgiler kaydedildi",
    "0x101": "%s kaydı bulunamadı",
    "0x102": "%s kaydı zaten mevcut"
})


def getStatus(statusCode=0, message=""):
    try:
        print(status["0x%d" % statusCode] % (message if message else ""))
    except:
        pass
