# Gereksinimler
from models.database import getTable
from bs4 import BeautifulSoup
import requests

products = getTable("products")
# Product Service veritabanına ürün URL bilgilerinin kaydedilmesi
# silinmesi, güncellenmesi ve sorgulanması işlemlerini yapmaktadır


def addProduct(productURL):
    # Gelen product URL bilgisi eğer veritabanında yoksa
    # Product tablosuna kaydediliyor
    # Kayıt tabloda var mı
    has_row = products.find_one({"url": productURL})
    # Eklenen URL Id bilgisi varsayılan
    _id = 0
    # Herhangi bir URL kaydı olmamalı
    if has_row is None:
        _id = products.insert_one({"url": productURL}).inserted_id
        print('%s eklendi' % productURL)
    # Eklenen URL bilgisinin ID bilgisi geri döndürülüyor
    return _id


def getProducts(limit=10):
    # Bu fonksiyon o gün içinde tarama yapılmamış
    # ve belirli bir limitte ki kaydı veritabanından getirir
    pass


def addPriceOfProduct(options):
    # Options data içerisinde, ilgili ürüne ait son kayıtlar bulunmaktadır
    # İlgili ürüne ait URL daha önce taranmış ama bugün taranmamış olabilir
    # Bu yüzden buraya gelen data yeniymiş gibi de gelebilir.
    # Ancak biz her bir datayı kaydetmeden önce, daha evvelden bugüne ait taranmış
    # bir data varsa kaydetmiyoruz. Ayrıca son fiyat etiketi, gelen fiyat etiketlerinden farklı değilse de
    # herhangi bir şekilde kayıt işlemi gerçekleştirilmiyor.
    # ancak data aynıysa, aynı datanın okuma tarihi güncellenebilir.

    # Bu data içerisinde;
    # options.price : Kampanya sonrası fiyatı / Satış fiyatı
    # options.oldPrice : Kampanya öncesi fiyatı
    # options.extraPrice : Kasada indirim gibi ekstra fiyat indirimi
    # options.dayNow : Şuan ki gün
    # options.monthNow : Şuan ki ay
    # options.yearNow : Şuan ki yıl
    # options.updateDate : Şuan ki tarama tarihi.
    pass


def getProductDataFromURL(url, schema):
    # Bu fonksiyon bizim için gelen URL adresinden ilgili fiyatları bulup getirecek
    # Bulunan datalar belirli bir fonksiyona iletilecek
    # Bu fonksiyon sadece ilgili url'den bilgi alma işlemini yapmaktadır.

    # URL       : Bağlantı sağlanacak url adresi
    # Schema    : Bağlantı sağlanan url adresindeki dataların bakılacağı nesne tanımlamaları
    # Schema Bilgisi            
    # schema.currentPriceEl     : Ürünün indirimsiz halini gösteren element sınıfı/idsi
    # schema.discountPriceEl    : Ürünün indirim sonrası tutarını gösteren element sınıfı/idsi
    # schema.basketPriceEl      : Ürünün baskette indirim tanımı varsa o elementin sınıfı/idsi
    # schema.imageEl            : Ürünün görsel adresini tutan element sınıfı/idsi
    # schema.titleEl            : Ürünün adını veren elementin sınıfı/idsi
    # schema.breadcrumbsEl      : Ürünün kategorilerini tutan elementin sınıfı/idsi

    content = requests.get(url)
    source = BeautifulSoup(content.content, "lxml")

    pass
