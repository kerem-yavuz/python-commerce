# Gereksinimler
import pymongo
from models.status import getStatus

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["commerce"]
stores = mydb["stores"]


# Store Service
# Mağazalarımızla ilgili işlemlerin yapıldığı servis


def addStore(options):
    # Hepsiburada, Gittigidiyor, N11 ve dahası gibi mağazaların ilk kayıt işlemleri

    # (Senaryo)
    # Öncelikle URL kısmı kontrol ediliyor
    # Eğer aynı URL bilgisine ait mağaza varsa kaydetmiyoruz

    # Gelen URL bilgisiyle aynı kayıt varsa bul
    has_store = stores.find_one({"url": options["url"]})

    # Yoksa kaydı oluştur ve id değerini ver
    if(has_store is None):
        x = stores.insert_one(options)
        return x.inserted_id

    # Varsa iptal et
    else:
        # Gerekli uyarıyı yazdırıyoruz
        getStatus(102, "Mağaza")
        return 0
