from store_service import addStore

# Örnek bir mağaza oluşturuluyor
store_id = addStore(
    {
        "title": "Google Inc",
        "url": "https://www.boyner.com.tr/",
        "status": True,
    }
)

# Eğer kayıt başarılı olursa ID değeri, değilse 0 değeri döndürmesi bekleniyor
print(store_id)
