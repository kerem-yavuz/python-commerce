import xmltodict, json, requests
from threading import Thread
from download import getdownload

def connection():
  xmlURL = 'https://statics.boyner.com.tr/bynsitemap/product1.xml'
  content = requests.get(xmlURL)
  website_text = content.text
  webJson = json.dumps(xmltodict.parse(website_text))
  urlset = json.loads(webJson)
  urls = urlset['urlset']['url']
  for element in urls:
      url = element['loc']
      try:
        getdownload(url)
      except Exception as E:
        print(E)
        pass
      

dum = Thread(target = connection)
dum.start()