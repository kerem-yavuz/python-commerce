from bs4 import BeautifulSoup
import requests

def getdownload(url):
  r = requests.get(url)
  source = BeautifulSoup(r.content, "lxml")
  t = source.select('.black-v1')
  if len(t) > 0:
    title = ' '.join(t[0].text.split()).encode('utf-8')
    priceOld = source.select('.price-psfx')
    price = source.select('.price-payable')
    if len(priceOld) > 0:
      priceOld = priceOld[0].text
    else:
      priceOld = "0"
    if len(price) > 0:
      price = price[0].text
    else:
      price = "0"
    img = source.select('.zoom img')
    if len(img) > 0:
      img = img[0]['data-lazy']
    else:
      img = "resimyok"

    f = open("%s.json" % title ,"w+")
    f.write(title)
    f.close()
  else:
    print('bos')

